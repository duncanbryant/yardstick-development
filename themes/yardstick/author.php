<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('base/header.twig', $data); ?>

<section id="author-section">
	<div class="container" id="content" tabindex="-1">
		<div class="row">
			<div class="col-4">
				<header class="page-header author-header">

					<?php
					$curauth = ( isset( $_GET['author_name'] ) ) ? get_user_by( 'slug',
					$author_name ) : get_userdata( intval( $author ) );
					?>

					<?php if ( ! empty( $curauth->ID ) ) : ?>
						<?php echo get_avatar( $curauth->ID ); ?>
					<?php endif; ?>

				</header><!-- .page-header -->

			</div>
			<div class="col">
				<h1>About <?php echo $curauth->nickname; ?></h1>
				<?php if ( ! empty( $curauth->user_description ) ) : ?>
					<dd><?php echo esc_html( $curauth->user_description ); ?></dd>
				<?php endif; ?>
			</div>
		</div>

		<div class="row">
			<div class="col">

				<h2>Posts by <?php echo $curauth->nickname ?></h2>

				<!-- The Loop -->
				<?php if ( have_posts() ) : ?>

					<section id="author-posts">
						<?php while ( have_posts() ) : the_post(); ?>
							<div class="col"> 
								<?php
									$data['article'] = get_post(); 
									$data['article']->title = get_the_title();
									$data['article']->author = [
										'name' => get_the_author(),
										'link' => '/author/'.get_the_author_link()
									];
									$data['article']->thumbnail = get_the_post_thumbnail_url();
									$data['article']->link = get_the_permalink();
								?>

								<?php Timber::render("template-bank/post-content.twig", $data); ?> 
							</div>
						<?php endwhile; ?>
					</section>

				<?php else : ?>

					<section id="no-results">
						<header class="page-header">
							<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'understrap' ); ?></h1>
						</header>
						<div class="page-content">
							<p><?php esc_html_e( 'This archive is empty.','understrap' ); ?></p>
						</div>
					</section>

				<?php endif; ?>

					<!-- End Loop -->

			</div>
		</div> <!-- .row -->
	</div><!-- Container end -->
</section>

<?php Timber::render('base/footer.twig', $data); ?>
