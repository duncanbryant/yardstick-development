<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<div id="blog-sidebar">
			<h3>More posts</h3>

			<?php $query = new WP_Query(array(
				'post_type' => 'post',
				'posts_per_page' => 2,
				'post__not_in' => array(get_the_id())
			));

			if ($query->have_posts()):
				while($query->have_posts()): $query->the_post(); ?>

				<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

			    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'large' ); ?>


			        <div class="article-image" style="background-image: url('<?php echo $url ?>')">
			          <a href="<?php the_permalink(); ?>"></a>
			        </div>


			        <div class="article-content">
								<img src="<?php the_field('author_image') ?>" alt="author-image"/>

								<a href="<?php the_permalink(); ?>">

			          <h4><?php the_title(); ?></h4>
			          <span class="btn btn-secondary">Read more</span>

								</a>

			        </div>

			  </article>

			<?php endwhile; endif; ?>

</div>
