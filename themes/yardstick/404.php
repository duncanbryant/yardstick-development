<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('base/header.twig', $data); ?>

<section id="error-404">
	<div class="container">
		<div class="row">
			<div class="col">
				<h1>Oops! That page can't be found.</h1>
				<h3><a href="<?php echo esc_url(get_home_url()); ?>">
						Back home
				</a></h3>
			</div>
		</div>
	</div>
</section>

<?php Timber::render('base/footer.twig', $data); ?>
