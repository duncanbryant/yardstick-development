//Return all charts (msut have .chart-container outer div)
var chartArray = [];
var socialProofSection = document.getElementById('social-proof');
var charts = document.querySelectorAll('.chart-container');
var animationTime = 1000;
var i = 0, target = 0;
var done = false;

//Check if section is on the page
if (socialProofSection) {

  //Add all Charts to chartArray with the custom fields accessible as properties
  while (i<charts.length){
    var a = {
      id : charts[i].children[0].id,
      stat : charts[i].getAttributeNode("data-stat").value,
      outof : charts[i].getAttributeNode("data-outof").value,
      number: charts[i].parentNode.lastElementChild
    }
    chartArray.push(a); a = {}; i++;
  };

  //Function to draw charts
  function drawCharts() {
    // if (socialProofSection) {
    //Only animate chart if the section is in view or scrolled into view
    if (!done && (document.documentElement.scrollTop + 0.75*window.innerHeight) >= socialProofSection.offsetTop) {
      //Stop charts from drawing more than once
      done = true;
      //Loop through charts and set attributes
      chartArray.forEach(function(chart) {
        //Draw chart
        var ctx = document.getElementById(chart.id).getContext('2d');
        var myChart = new Chart(ctx, {
          type: 'doughnut',
          data: {
            datasets: [{
              data: [chart.stat, (chart.outof - chart.stat)],
              borderWidth: [0,0],
              backgroundColor: ['#23313d','#91cbb3'],
              hoverBackgroundColor: ['#23313d','#91cbb3'],
            }],
          },
          options: {
            legend: {
              display: false,
            },
            tooltips: {
              enabled: false,
            },
            animation: {
              easing: 'easeInOutSine',
              duration: animationTime
            },
            responsive: true,
            maintainAspectRatio: false,
            cutoutPercentage: 75
          }
        });
        //Animate number
        var number = 0, target = (chart.stat / chart.outof)*100, iteration = animationTime / target;
        var interval = setInterval(function() {
          chart.number.textContent = number;
          if (number >= target) clearInterval(interval);
          number++;
        }, iteration);
      });
    }
  }//End of draw chart function

  //Draw when loaded or when scrolled to
  window.addEventListener("load",drawCharts,{ passive: true });
  window.addEventListener("scroll",drawCharts,{ passive: true });
  // }
}