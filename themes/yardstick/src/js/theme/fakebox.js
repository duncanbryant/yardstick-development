(function ( $ ) {

    /**
    Want to make your checkboxes fancy? Well now you can!
    $('.elem').fakebox(); is all you need. This will give a basic box and label, assuming there's one next to the checkbox you targetted
    $('.elem').fakebox({
        checkboxContent: '<i class="far fa-check"></i>'
    }); will put a fontawesome checkbox in your checkbox, you can put anything in there though; images, text, etc. - Though you may want to edit the size of the checkbox based on your needs - see: components/fakebox.scss
    $('.elem').fakebox({
        labelClass: 'text-large'
    }); Got a generic class for text? Use it.
    $('.elem').fakebox({
        replaceParent: false
        label: 'Click me!'
    }); Are you not replacing a checkbox which is already there (e.g. Contact Form 7 checkbox), then you may not want to remove the checkbox's parent
    */
 
    $.fn.fakebox = function( options ) {

        var settings = $.extend({
            // These are the defaults.
            label: null,
            replaceParent: true, //If you aren't setting a label, this is advised
            checkboxContent: '',
            fakeboxClass: '',
            checkboxClass: '',
            labelClass: '',
            checkboxContent: '',
        }, options );


    	this.each(function(){
        	var checkbox = $(this);
        	var value = checkbox.attr('value');
            var checked = checkbox.prop('checked');
            var name = checkbox.attr('name');


            var elem = '<label class="fakebox '+settings.fakeboxClass+'" for="'+name+'">';
                elem += '<input name="'+name+'" id="'+name+'" type="checkbox"';
                if(checked){
                    elem += 'checked="checekd"';
                }
                elem += '>';
                elem += '<div class="checkbox '+settings.checkboxClass+'">'+settings.checkboxContent+'</div>';
                if(checkbox.parent().is('label') && settings.label == null){
                    elem += '<div class="label">'+checkbox.parent().html().replace(checkbox[0].outerHTML, '')+'</div>';
                }
                if(checkbox.parent().find('label').length && settings.label == null){
                    elem += '<div class="label">'+checkbox.parent().find('label').html()+'</div>';
                }
                if(settings.label != null){
                    elem += '<div class="label '+settings.labelClass+'">'+settings.label+'</div>';
                }
            elem += '</label>';

            if(settings.replaceParent){
                checkbox.parent().replaceWith(elem);
            }else{
        	   checkbox.replaceWith(elem);
            }
        });
        

        return true;
    };

    $('.makeFakebox').fakebox();
 
}( jQuery ));

