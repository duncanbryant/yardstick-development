(function ( $ ) {
	/**
	So you're going to be embedding YouTube videos, or maybe you need a div to have a specific aspect ratio?
	That's all well and good, but that width/height attribute that YouTube spits out isn't very responsive
	.aspectRatio() those videos, or any other element, to set the height, based on the width.

	Example:
	$('elem').aspectRatio(); - Set the element to have 9px height for every 16px width (default video ratio)
	$('elem').aspectRatio({
		width: 100,
		height: 15
	}); - Set the element to have 15px of height for every 100px width

	*/
	$.fn.aspectRatio = function( options ) {

		var settings = $.extend({
			// Default to a 16:9 aspect ratio; a "normal" video
			width: 16,
			height: 9
		}, options );

		var w = parseInt(settings.width);
		var h = parseInt(settings.height);
		var calc = h/w; //What you * the width by to get the height - everything is based on the width


		
		return this.each(function() {
			//Set the height to the width * calc value
			$(this).removeAttr('height');
			$(this).removeAttr('width');
			$(this).height(($(this).width() * calc));
		});
	 
	};

	$('iframe[src*=".youtube."]').aspectRatio();
	$(window).on('resize', function(){
		$('iframe[src*=".youtube."]').aspectRatio();
	});
}( jQuery ));