'use strict';

//JQUERY//
jQuery(document).ready(function ($) {

      $("#mobile-menu").mmenu({
         // options
      }, {
         // configuration
         offCanvas: {
            pageNodetype: "section",
            pageSelector: "#my-meenu-wrapper"
         }
      });

      //Append close button
      var $html = '<a id="menu-close" class="menu-toggle" href="#mobile-menu"><i class="fas fa-times"></i></span></a>';
      var mmenu = $("#mobile-menu").data('mmenu');
      $('#mobile-menu').append($html);
      $('.menu-toggle').on('click', function(){
         mmenu.open();
      });
      $('#menu-close').click(function(){mmenu.close();});

});

//JAVASCRIPT///
