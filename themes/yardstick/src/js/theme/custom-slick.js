'use strict';

//JQUERY//
jQuery(document).ready(function ($) {

	//Homepage slider
	$('#homepage-slider').slick ({
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<button class="prev"><i class="fas fa-chevron-left"></i></button>',
		nextArrow: '<button class="next"><i class="fas fa-chevron-right"></i></button>',
		dots: true,
		pauseOnHover: false
	});

	//Bucket carousel
	$('#buckets-carousel').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		prevArrow: '<button class="prev"><i class="fas fa-chevron-left"></i></button>',
		nextArrow: '<button class="next"><i class="fas fa-chevron-right"></i></button>'
	});

	//Testimonial slider
	$('#testimonial-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: true,
		dots: false,
		prevArrow: '<button class="prev"><i class="fas fa-chevron-left"></i></button>',
		nextArrow: '<button class="next"><i class="fas fa-chevron-right"></i></button>',
		pauseOnHover: false
	});

	$('.portfolio-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: true,
		dots: false,
		prevArrow: '<button class="prev"><i class="fas fa-chevron-left"></i></button>',
		nextArrow: '<button class="next"><i class="fas fa-chevron-right"></i></button>',
		pauseOnHover: false
	});

	$('#author-posts').slick({
		slidesToShow:2,
		slidesToScroll: 1,
		prevArrow: '<button class="prev"><i class="fas fa-chevron-left"></i></button>',
		nextArrow: '<button class="next"><i class="fas fa-chevron-right"></i></button>',
		dots: false
	});

	$('.spotlight-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		prevArrow: '<button class="prev"><i class="fas fa-chevron-left"></i></button>',
		nextArrow: '<button class="next"><i class="fas fa-chevron-right"></i></button>',
		dots: false,
		pauseOnHover: false
	});

});

//JAVASCRIPT///
