<?php
/**
* Understrap functions and definitions
*
* @package understrap
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$understrap_includes = array(
	'/setup.php',                           // Theme setup and custom theme supports.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/template-tags.php',                   // Custom template tags for this theme.
);

foreach ( $understrap_includes as $file ) {
	$filepath = locate_template( '/inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

//Add thumbnail functionality for posts
add_theme_support( 'post-thumbnails',array('post'));

//Global options on admin sidebar
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Global Options',
		'menu_title'	=> 'Global Options',
		'menu_slug' 	=> 'global-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Testimonials',
		'menu_title'	=> 'Testimonials',
		'menu_slug' 	=> 'testimonials',
		'capability'	=> 'edit_posts',
		'icon_url' => 'dashicons-groups',
		'redirect'		=> false
	));
}

//Remove comments page
function remove_menus(){
	remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );

//String to lowercase and hyphenated
if( ! function_exists('to_hyphenated') ) {
	function to_hyphenated($string = "") {
		return strtolower(preg_replace("/[^\w]+/", "-", $string));
	}
}

//Get testimonials
if( ! function_exists('get_testimonials') ) {
	function get_testimonials($a = 'general') {
		//See if $attr is a choice in acf
		$attr = strtolower($a);
		if (in_array($attr, array_flip(get_field_object('field_5bfe9c875673e')['choices']))) {
			//include(locate_template('page-sections/site-wide/testimonials.php'));
			$data = pageData();
			$data['attr'] = $a;
			Timber::render('page-sections/site-wide/testimonials.twig', $data);
		}
		else {
			echo "Choice doesn't match ACF attribution";
		}
	}
}

//Pagination
if( ! function_exists('paginate') ) {
	function paginate( $a_query) {

		$pagination = paginate_links( array(
			'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			'total'        => $a_query->max_num_pages,
			'current'      => max( 1, get_query_var( 'paged' ) ),
			'format'       => '?paged=%#%',
			'show_all'     => false,
			'type'         => 'plain',
			'end_size'     => 2,
			'mid_size'     => 1,
			'prev_next'    => true,
			'prev_text'    => sprintf( '<i class="fas fa-chevron-left" aria-hidden="true"></i> %1$s', __( 'Newer', 'text-domain' ) ),
			'next_text'    => sprintf( '%1$s <i class="fas fa-chevron-right" aria-hidden="true"></i>', __( 'Older', 'text-domain' ) ),
			'add_args'     => false,
			'add_fragment' => '',
		) );

		echo '<div id="pagination">'. $pagination .'</div>';
	}
}


//Shorthand function to check variables
function dd($var){
	var_dump($var);
	die();
}

//Shorter method for getting image custom field values when using image array when using image array custom field
function getImage($customField, $size = null){
	if(empty($size)){
		return $customField['url'];
	}

	return $customField['sizes'][$size];
}

//Get image tag with all fields filled in when using image array custom field
function getImageTag($customField, $size = null, $attrs = []){
	echo '<img src="'.getImage($customField, $size).'" '.((!empty($customField['alt']) && !isset($attrs['alt'])) ? 'alt="'.$customField['alt'].'" ' : ' ');
	foreach($attrs as $attr => $value){
		echo $attr.'="'.$value.'" ';
	}
	echo '>';
}

function registerNewMenus() {
	register_nav_menus(
		[
		 	'top-menu' => __( 'Top Menu' ),
			'footer-menu' => __( 'Footer Menu' ),
			'policy-menu' => __( 'Policy Menu' ),
		]
	);
}
add_action( 'init', 'registerNewMenus' );

function pageData(){
	$data = Timber::get_context();
	$data['post'] = new TimberPost();
	$data['main_menu'] = new Timber\Menu("primary-menu");
	$data['top_menu'] = new Timber\Menu("top-menu");
	$data['footer_menu'] = new Timber\Menu("footer-menu");
	$data['policy_menu'] = new Timber\Menu("policy-menu");
	$data['options'] = get_fields('options');
	return $data;
}

function matchTestimonialAttr($match, $attr = 'general'){
	return in_array($attr, $match);
}

function testimonialClientSince($author, $since = null){
	if ($since){
		$author = explode(',' , $author);
		//Change 'A client since' if there are more than 1 clients
		$text = (strpos($author[0],' and ') || strpos($author[0],' & ') ) ? 'Clients since' : 'A client since'; 
	}
										
	if($since){
		return '<p class="client-since">'.$text . ' ' . $since.'</p>';
	}
}

function fcRow($content){
	$row = strtolower(preg_replace("/[^A-Za-z0-9]/", '-', $content['acf_fc_layout']));
    $row = explode('-', $row);

    foreach($row as $index => $r){
        if(empty($r)){
            unset($row[$index]);
        }
    }
    $row = implode('-', $row);

	Timber::render('page-sections/flexible-content/fc-sections/'.$row.'.twig', $content);
}

function getPostsFromParent($parentID){
	return Timber::get_posts(
		array (
			'post_type' => 'page',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'post_parent' => $parentID,
			'posts_per_page' => -1
		)
	);
}


/** 
Timber Functions
=-=-=
To call functions in a twig file, you need to register like below
*/

add_filter( 'timber/twig', function( \Twig_Environment $twig ) {
    $twig->addFunction( new Timber\Twig_Function( 'dd', 'dd' ) );
    $twig->addFunction( new Timber\Twig_Function('get_the_title', 'get_the_title'));
    $twig->addFunction( new Timber\Twig_Function('to_hyphenated', 'to_hyphenated'));
    $twig->addFunction( new Timber\Twig_Function('get_testimonials', 'get_testimonials'));
    $twig->addFunction( new Timber\Twig_Function('matchTestimonialAttr', 'matchTestimonialAttr'));
    $twig->addFunction( new Timber\Twig_Function('testimonialClientSince', 'testimonialClientSince'));
    $twig->addFunction( new Timber\Twig_Function('do_shortcode', 'do_shortcode'));
    $twig->addFunction( new Timber\Twig_Function('getImage', 'getImage'));
    $twig->addFunction( new Timber\Twig_Function('getImageTag', 'getImageTag'));
    $twig->addFunction( new Timber\Twig_Function('fcRow', 'fcRow'));
    $twig->addFunction( new Timber\Twig_Function('getPostsFromParent', 'getPostsFromParent'));
    return $twig;
} );