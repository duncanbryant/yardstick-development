#Information for the ys-boiler-template theme

Follow the links in the contents for any information about the boiler template theme

##Contents

1. [Folder structure](#markdown-header-1-structure)
    * [/assets](#markdown-header-11-assets)
    * [/css](#markdown-header-12-css)
    * [/fonts](#markdown-header-13-fonts)
    * [/inc](#markdown-header-14-inc)
    * [/js](#markdown-header-15-js)
    * [/sass](#markdown-header-16-sass)
    * [/views](#markdown-header-17-views)
    * [/src](#markdown-header-18-src)
2. [FTP](#markdown-header-2-flywheel)
3. [External Libraries](#markdown-header-3-external-libraries)
    * [FontAwesome](#markdown-header-31-fontawesome)
    * [Slick](#markdown-header-32-slick)
    * [MMenu](#markdown-header-33-menu)

##1. Folder structure
A brief explanation of the folders within the template theme

###1.1 /assets
For any files that must be used that can't be uploaded to WordPress e.g SVGs

###1.2 /css
Where the **`/sass/theme.scss`** is compiled into **`theme.css`** and minified into **`theme.min.css`**, the min.css is file is the one that the site uses.

###1.3 /fonts
All the font files for FontAwesome Pro. Google Fonts are enqueued in **`/inc/enqueue.php`**

###1.4 /inc
Contains default Understrap functions as well as Bootstrap Navwalker and the enqueue file.

###1.5 /js
Where the all .js files in **`/src/js`** are compiled into **`theme.js`** and minified into **`theme.min.js`**, the min.css is file is the one that the site uses.

###1.6 /sass
The sass files are split into 5 folders:

- **`/sass/assets`** - for styles needed for external libraries e.g Bootstrap or Slick
- **`/sass/pages`** - for page specific styles (nest the whole file inside of a unique page selector e.g **`#client-stories {}`** or **`.page-id-120 {}`**)
- **`/sass/sections`** - for sections specific styles
- **`/sass/components`** - for element/group of element specific styles
- **`/sass/variables`** - for variables and mixins

(Styles used to be in the form **`_style.scss`** but the underscore is no longer necessary).

All sass files need to be imported into the **`/sass/theme.scss`** file.

All sass files are placeholders, do not feel constrained by them. If you have a new component for you site, add a new file for it.

###1.7 /views

Timber looks in the views folder for all twig files, both through `Timber::render()` and `{% include %}`, putting all view-related files in here has the added benefit of knowing there should be no code that affects data on screen (beyond styling) in the files inside. 

#### /views/base

These are the base files for the template; header, body, and footer.

`base.twig` is the file that all other templates will extend, as it includes both the header and footer; meaning you don't need to include wp_head/wp_footer on every template. 

#### /views/page-sections

If you have a large block of code in a template, or a block of code which will be used on multiple pages, e.g. a hero banner, you may want to put that code in a file here.

The flexible-content directory, or it's sub-directory fc-sections, should not be modified unless you have altered the Default Page custom fields. If you have added a new flexible content section, name the file `your-custom-content-section.twig` the exact same as in the custom field, and it will be automatically picked up.

When styling site-wide sections, style them in their own sass file in **`/sass/sections`**, then for every page they're on, if they need additional styling, add those styles to the page sass file in **`/sass/pages`**

#### /views/template-bank

The template files in here are very similar to page-sections. The difference is these are less likely to be used in your site, so you may not need them.

#### /views/templates

These are the overall view files for each template.

###1.8 /src
This src folder is split into 2 folders:

#### /src/js
The JS folder is split into 3 folders:

- **`/js/assets`** - for JS files needed for external libraries
- **`/js/theme`** - for theme JS files that you edit
- **`/js/template-bank`** - for theme JS files that you may not need, but may edit

#### /src/sass
This folder just contains the sass files copied from node modules downloaded during **`npm install`**

##2. Flywheel

All interactions between a site and the code should be done through Flywheel; database changes, theme changes, etc.


###3 External libraries
Default file info for external libraries

###3.1 FontAwesome
The template uses FontAwesome 5. Icons can be added through SASS as well as in the HTML.

See the [SASS documentation](https://fontawesome.com/how-to-use/on-the-web/using-with/sass).

###3.2 Slick
By default, all sliders are initialised within **`/src/js/theme/custom-slick.js`**. See the [documentation](http://kenwheeler.github.io/slick/).

There are a few mixins to add default styles to slick sliders, which are found in **`/sass/variables/theme-slick.scss`**.

E.g

```sass

  #testimonial-section {
    //Slick arrows
    button.slick-arrow {
      @include default-slick-controls;
    }

    //Slider dots
    ul.slick-dots {
      //Dots
      @include default-slick-dots;
      //Lines
      //@include default-slick-lines;
    }
  }

```

###3.3 MMenu
The settings for MMenu are found in **`/src/js/theme/custom-mmenu.js`**, with the styles in **`/sass/sections/mobile-menu.scss`**.

See the [documentation](https://mmenu.frebsite.nl/).
