<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('templates/post.twig', $data);

?>
