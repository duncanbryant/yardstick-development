<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('base/header.twig', $data);
?>

<!-- Hero -->
<?php get_template_part('page-sections/site-wide/hero'); ?>

<div id="blog-posts">
	<div class="container" id="content" tabindex="-1">
		<div class="row">
			<div class="col-12 col-md-8">

				<header>
					<?php //Get archive description if it exists
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );?>
				</header>

				<div id="post-wrapper">

					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>

							<?php 
								$data['article'] = get_post(); 
								$data['article']->title = get_the_title();
								$data['article']->author = [
									'name' => get_the_author(),
									'link' => '/author/'.get_the_author_link()
								];
								$data['article']->thumbnail = get_the_post_thumbnail_url();
								$data['article']->link = get_the_permalink();
							?>

							<?php Timber::render("template-bank/post-content.twig", $data); ?>

						<?php endwhile; ?>


					<?php else : ?>

						<section id="no-results">
							<header class="page-header">
								<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'understrap' ); ?></h1>
							</header>
							<div class="page-content">
								<p><?php esc_html_e( 'This archive is empty.','understrap' ); ?></p>
							</div>
						</section>

					<?php endif; ?>

				</div>

				<?php paginate($wp_query); ?>

			</div>

			<div class="col-12 col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</div>

<?php Timber::render('base/footer.twig', $data); ?>
