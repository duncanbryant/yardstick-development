<?php //Template name: Contact
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('templates/contact.twig', $data);
?>
