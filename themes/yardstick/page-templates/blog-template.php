<?php //Template name: Blog
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
$data['posts'] = new Timber\PostQuery(
	array (
		'post_type' => 'post',
		'orderby' => 'date',
		'order' => 'ASC',
		'posts_per_page' => 6,
		'paged' => $paged
	)
);
Timber::render('templates/blog.twig', $data);

?>
