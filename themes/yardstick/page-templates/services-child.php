<?php //Template name: Services Child
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('templates/services-child.twig', $data);
?>
