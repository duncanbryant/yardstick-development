<?php //Template name: Homepage
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
$data['posts'] = new Timber\PostQuery(
	array (
		'post_type' => 'post',
		'orderby' => 'date',
		'order' => 'ASC',
		'posts_per_page' => 3,
		'paged' => $paged
	)
);
$data['clients'] = new Timber\PostQuery(
	array (
		'post_type' => 'page',
		'order' => 'ASC',
		'post_parent' => 562,
		'posts_per_page' => -1
	)
);
Timber::render('templates/home.twig', $data);
