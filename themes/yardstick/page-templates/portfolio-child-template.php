<?php //Template name: Portfolio Single
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$singular = get_field('singular_name');
$singular .= "'" . ((substr(get_field('singular_name'), -1) == 's') ? '' : 's');

$data = pageData();
$data['singular'] = $singular;
Timber::render('templates/portfolio-child.twig', $data);
?>
