<?php //Template name: Portfolio Template
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
$data = pageData();
$data['client_portfolio'] = Timber::get_posts(
	array (
		'post_type' => 'page',
		'post_parent' => 562,
		'posts_per_page' => -1
	)
);
Timber::render('templates/portfolio.twig', $data);
?>
