<?php //Template name: Client persona
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('templates/client-persona.twig', $data);
?>
