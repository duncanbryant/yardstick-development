<?php //Template name: Services
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('templates/services.twig', $data);
?>
