<?php //Template name: Why Choose Us
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('templates/why-choose-us.twig', $data);
?>
