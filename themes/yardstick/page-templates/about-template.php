<?php //Template name: About Template
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
$data = pageData();
$data['team_members'] = Timber::get_posts(
	array (
		'post_type' => 'page',
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'post_parent' => 528,
		'posts_per_page' => -1
	)
);
Timber::render('templates/about.twig', $data);
?>
