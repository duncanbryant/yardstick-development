<?php //Template name: Team Member Single
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$singular = get_field('singular_name');
$singular .= "'" . ((substr(get_field('singular_name'), -1) == 's') ? '' : 's');

$data = pageData();
$data['singular'] = $singular;
Timber::render('templates/team-single.twig', $data);
?>