#Yardstick Agency Boiler Template

Information in this readme:

1. Timber and Twig - Explaining the overall specifics of this theme in detail
2. General Info - Explaining setting up the site in detail

##Timber and Twig

####The basics of Timber, and the MVC concept

Timber and Twig are made with the intention of removing The Loop from Wordpress, whilst also making it more MVC-like.

To make Wordpress more MVC like, the template .php files are now treated as controllers, and is where any and all PHP code should be done, then passed from there to view files, where the only thing there should be HTML, variables, and logic (if/else, while, etc. statements)

To call a view file, and pass variables to it, you need to use the following `Timber::render('path/to/file.twig', [ARRAY OF VARIABLES]);` where path/to/file.twig is how to get to the twig file once you have opened the views folder - e.g. base.twig would be `base/base.twig` and the `ARRAY OF VARIABLES` is all the variables you want to use on that page. Unless you need to add any extra PHP code, using `pageData()` will give you all of the data you need. So to include base.twig, with all the basic data needed, we have the following in our template file

```
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
Timber::render('base/base.twig', $data); 
?>
```

If we needed to pass today's date through, as a variable, we can add it to `$data`:

```
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$data = pageData();
$data['date'] = date('d/M/Y');
Timber::render('base/base.twig', $data); 
?>
```

####Variables, logic, and comments

Twig files do not accept PHP, instead they have their own syntax.

#####Variables

Variables are called by wrapping the variable name, defined as the array key passed from the template (see last code block in previous section), in double braces: `{{ variable }}` which will do the same as `<?php echo $variable; ?>`

If your variable is an object or an array, and you want to call a specific child, you call it like so: `{{ array.key }}` which will do the same as `<?php echo $array['key']; ?>`


#####Logic

Functional code, usually logic, is wrapped in a single brace and percentage symbol.

```
{% if variable %}
	...
{% endif %}
```

The logic blocks use a similar format to PHPs, meaning you can do if, while, for, etc.

However, due to the need for The Loop with Timber, the while loop is also no longer as prevelant. Instead, you will be using the for loop more.

For example, if you have a variable of array, which you want to have as list items

```
<ul>
	{% for value in array %}
		<li>{{ value }}</li>
	{% endfor %}
</ul>
```

But what if you wanted to number them, for example a slider with slide numbers?

```
<div class="slider">
	{% for index,slide in slides %}
		<div class="slide" data-index="{{ index }}">
			<h1>{{slide.title}}</h1>
		</div>
	{% endfor %}
</div>
```

Do note that the index is 0 indexed, so if you wanted data-index to start at 1, you could need to do `{{ index + 1}}`

Running PHP functions in Twig can be done in two ways; setting a Timber function, or by running using `{{ function('function_name', parameter) }}`

If you are going to be using that function just once, the latter makes the most sense:
```
<head>
	{{ function('wp_head') }}
</head>
```

If the function should be easily accessible, then it should be registered as a Timber function. For code example, see the bottom of `functions.php`

The last thing to note with functional code is setting variables. Whilst all variables should be set in the php file, and not in the twig file, there may be a case where you require to update a view file based on something you have looped over, for example custom field values. To create a variable in twig, you use `set` inside a functional code block.

```
{% set variable = 'value' %}
```

So if we wanted to use that variable, and have it contain all team members, we may do the following.

```
{% set members = '' %}
{% for person in team_members %}
	{% set members = members + person.name + ', ' %}
{% endfor %}
<h1>A big welcome from {{ function('rtrim', members, ', ') }}</h1>
```

#####Comments

Commenting in twig is done with a single brace and hash.

```
{# This is a comment #}
```

Commenting in this fashion should be preferred over HTML comments, as they do not show in the DOM.

####Getting data from Wordpress, and using it

Using the `pageData()` function you will get data for the current page/post, including custom fields, options page's custom fields, and the 4 predefined menus, by default.

Page/post data is all called from the `post` variable.

```
<h1>{{ post.title }}</h1>
<h2>{{ post.subtitle }}</h2> {# this is a custom field #}
```

Options page data is called with the `options` variable.

```
<a href="tel:{{ options.phone_number }}">Call us</a>
```

And the menus are called by their semantic names:

```
{{ top_menu }}
{{ main_menu }}
{{ footer_menu }}
{{ policy_menu }}
```

To see menu usage, see `/views/page-sections/site-wide/desktop-top-bar.twig`

######Images

When dealing with images, you can use the `Image()` function which will return the image URL, even if the custom field return the image as an object. But with this, you can also use Timber's built in resize functionality.

```
{# post.image is 1600px wide, but we only need a 500px image here #}
<img src="{{ Image(post.image) }}"> {# returns a 1600px image - would need to resize this in Photoshop to reduce load time #}
<img src="{{ Image(post.image)|resize(500) }}"> {# returns a 500px wide image, allowing us to use the same image at a different size elsewhere if needed #}
```

######Quirks

Whilst all custom fields on a post can be called with `post.FIELD_NAME` doing so on a repeater will return the count of items in the repeater. This is done so that `{% if post.repeater %}` will fail if the repeater is empty.

As such, if you need to get a repeater, you will need to do `post.get_field('repeater')` but only repeaters require this.


##General info

General info for the boiler template.

For info about the theme itself click [here](https://bitbucket.org/theyardstickagency/ys-boiler-template/src/master/themes/ys-boiler-template/)

##Getting started

(This guide will use EXAMPLE in place of the name of the site and repo)

###Start with Flywheel

1. Open Local by Flywheel, and create a new site with the name EXAMPLE.
2. Once the site has finished creating, press the arrow next to the file path, and File Explorer will open you in the correct folder.
3. Navigate to EXAPLE/app/public/wp-content, and delete the contents of this directory.

###Download necessary files

Go to the [Downloads page](https://bitbucket.org/theyardstickagency/ys-boiler-template/downloads/) and download:

1. The repository
2. The database: **`theyards_with-new-custom-fields.sql`**


###Setting up the repo

1. Create a new repository in Bitbucket for EXAMPLE
2. Open Sourcetree, and clone EXAMPLE to the empty wp-content folder from the Flywheel section
3. Extract the repository zip file, downloaded from the previous section, into the empty wp-content directory after Sourcetree has finished cloning the repository. (If you have hidden files/folders enabled, you will see a .git folder)
4. Once all files have extracted into wp-content, go back to Sourcetree and create the initial commit, and push.


###Uploading database file

1. Open Local by Flywheel, and go to the Database tab for EXAMPLE
2. Press the adminer button
3. Press "Import" in the sidebar of the webpage which appears
4. Upload the database file downloaded in the second section
5. Press "SQL Command" in the sidebar and copy the code below into the query box (change EXAMPLE to the new website's folder name)
6. Click "Execute" and it will find and replace the default URLs with your site's

*SQL query:*

```sql

UPDATE wp_options SET option_value = replace(option_value, 'http://boilerplate.local', 'http://EXAMPLE.local') WHERE option_name = 'home' OR option_name = 'siteurl';

UPDATE wp_posts SET guid = replace(guid, 'http://boilerplate.local','http://EXAMPLE.local');

UPDATE wp_posts SET post_content = replace(post_content, 'http://boilerplate.local', 'http://EXAMPLE.local');

UPDATE wp_postmeta SET meta_value = replace(meta_value,'http://boilerplate.local','http://EXAMPLE.local');

```

###Logging into WordPress and changing the username

1. The username and password are 'duncan' & 'monkey123'
2. To change the username to your own, go to Adminer (in the previous section)
3. Go to **`wp_users`**
4. Edit the user and change every instance of 'gilad' to your name (e.g **`user_login`**, **`user_nicename`**, **`user_email`** & **`display_name`**)
5. Log back into Wordpress with your username and 'monkey123' as the password

###Installing Node

1. Change the theme folder from **`themes/boiler`** to **`themes/YOUR-SITE-NAME`**
2. Open **`themes/new-website/style.css`** and change the theme name and description
3. Open the terminal on the theme folder and run **`npm install`**
4. Once it's done, log into WordPress and activate the theme again

##THE SITE'S NOT SHOWING!?
If you are seeing a blank page, or just an error message, ensure the Timber plugin is enabled, by logging in to the admin panel. This plugin needs to be active for the twig files, used to display pages, to be recognized.

After the previous steps have been completed, [follow this link](https://bitbucket.org/theyardstickagency/ys-boiler-template/src/master/themes/ys-boiler-template/) for information about the theme itself.
